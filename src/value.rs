#[derive(Debug, Clone, PartialEq)]
pub enum Value {
    I32(i32),
    Bool(bool),
    Argc(usize),
    Effect(usize, usize), //type, argc
    String(String),       //TODO
    //Unit
}

impl Value {
    pub fn expect_effect(&self) -> (usize, usize) {
        match self {
            Value::Effect(_type, argc) => (*_type, *argc),
            v => panic!("Expected effect, found {:?}", v),
        }
    }

    pub fn expect_argc(&self) -> usize {
        match self {
            Value::Argc(argc) => *argc,
            v => panic!("Expected argc, found {:?}", v),
        }
    }

    pub fn expect_i32(&self) -> i32 {
        match self {
            Value::I32(i) => *i,
            v => panic!("Expected i32, found {:?}", v),
        }
    }

    pub fn expect_bool(&self) -> bool {
        match self {
            Value::Bool(b) => *b,
            v => panic!("Expected bool, found {:?}", v)
        }
    }
}

impl From<Value> for bool {
    fn from(val: Value) -> bool {
        match val {
            Value::Bool(b) => b,
            v => panic!("Expected bool, found {:?}", v),
        }
    }
}

use std::fmt;

impl fmt::Display for Value {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Value::I32(i) => write!(f, "{}i", i),
            Value::Bool(true) => write!(f, "True"),
            Value::Bool(false) => write!(f, "False"),
            Value::Argc(a) => write!(f, "__argc({})", a),
            Value::Effect(e, a) => write!(f, "<eff:{}-{}>", e, a),
            Value::String(s) => write!(f, "\"{}\"", s),
            //Value::Unit => write!(f, "()")
        }
    }
}
