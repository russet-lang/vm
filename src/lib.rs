#[macro_use]
extern crate lazy_static;

pub mod value;

pub mod fiber;
pub mod vm;

pub use crate::vm::*;
use fibers::{Executor, ThreadPoolExecutor, executor::ThreadPoolExecutorHandle};

use termion::{color, style};
use core::bytecode::Bytecode;

lazy_static! {
    static ref GRAY : color::Fg<color::AnsiValue> = color::Fg(color::AnsiValue::grayscale(10));
    static ref RED : color::Fg<color::AnsiValue> = color::Fg(color::AnsiValue::rgb(4, 1, 1));
}

/// Only for documentation purpouses
pub fn vm_with_threadpool<'a>(code: Vec<Bytecode>)
                              -> Vm<'a, ThreadPoolExecutorHandle> {
    let executor = ThreadPoolExecutor::new()
        .expect("Cannot create thread pool");

    Vm::new(executor.handle(), code)
}

/// Runs new Virtual Machine on thread pool
///
/// # Examples
///
/// Simple running vm
///
/// ```rust
/// use core::bytecode::Bytecode;
///
/// vm::vm(vec![Bytecode::PushI32(42)]);
/// ```
pub fn vm(code: Vec<Bytecode>) {
    use crate::fiber::wait_for::Event;
    use crate::value::Value;
    use futures::Async;

    let mut executor = ThreadPoolExecutor::new()
        .expect("Cannot create thread pool");

    let mut vm = Vm::new(executor.handle(), code);

    vm.add_handler(
        0,
        Box::new(|tx, values| {
            println!("{}{}{}", style::Bold, values[0], style::Reset);
            tx.send(Event::Continue(vec![Value::Argc(0)]))
                .expect("[core] Cannot send continue");
            Async::NotReady
        }),
    );

    vm.add_handler(
        1,
        Box::new(|tx, _| {
            use std::io;
            println!("{}[core] ### READ{}", *GRAY, style::Reset);

            let mut input = String::new();
            if let Err(err) =  io::stdin().read_line(&mut input) {
                println!("{}[core]Cannot read line: {}", *RED, err);
                return Async::Ready(());
            }

            let input = input.trim_right().to_string();
            tx.send(Event::Continue(vec![
                Value::Argc(1),
                Value::String(input)
            ])).expect("[core] Cannot send continue");
            Async::NotReady
        }),
    );

    //TODO: TMP add1
    vm.add_handler(2, Box::new(|tx, values| {
        let mut value = values[0].expect_i32();
        value += 1;
        tx.send(Event::Continue(vec![Value::I32(value)]))
            .expect("[core] Cannot send continue");
        Async::NotReady
    }));
    vm.run();

    executor
        .run_future(vm)
        .expect("Cannot run executor")
        .expect("Fiber had problems");
}
