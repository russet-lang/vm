use std::sync::Arc;
use fibers::{sync::mpsc, Spawn};
use futures::Async;
use uuid::Uuid;
//use termion::{color, style};

pub mod future;
pub mod wait_for;

use self::wait_for::{Event, WaitFor};
use core::bytecode::Bytecode;
use crate::value::Value;
use crate::vm::SourceCode;

pub enum State {
    Running,
    Waiting(WaitFor),
}

pub struct Fiber<H> {
    id: Uuid,
    pc: usize,
    stack: Vec<Value>,

    code: Arc<SourceCode>,

    //Debug
    //color: color::AnsiValue,

    //Future
    handle: H,
    state: State,

    //MPSC
    parent_context: Option<Context>,
    child_context: Option<Context>,
}

impl<H> Fiber<H>
where
    H: Spawn + Clone + Send + 'static,
{
    pub fn new(pc: usize, handle: H, code: Arc<SourceCode>) -> Self {
        //use rand::Rng;
        //let r = rand::thread_rng().gen_range(2, 5);
        //let g = rand::thread_rng().gen_range(2, 5);
        //let b = rand::thread_rng().gen_range(2, 5);
        Fiber {
            id: Uuid::new_v4(),
            pc,
            stack: vec![],
            code,

            //color: color::AnsiValue::rgb(r, g, b),

            handle,
            state: State::Running,

            parent_context: None,
            child_context: None,
        }
    }

    pub fn with_parent(mut self, context: Context) -> Self {
        self.parent_context = Some(context);
        self
    }

    pub fn boxed(self) -> Box<Self> {
        Box::new(self)
    }

    pub fn run(&mut self) -> Result<Async<()>, ()> {
        loop {
            if let Some(s) = self.run_once() {
                return Ok(s);
            }
        }
    }

    pub fn run_once(&mut self) -> Option<Async<()>> {
        let bytes = &self.code.0;
        let byte = bytes.get(self.pc);
        if byte.is_none() {
            return Some(Async::Ready(()));
        }
        let byte = byte.expect("next byte");

        //println!("{}[{}; {}] Byte: {:?}{}",color::Fg(self.color), self.id, self.pc, byte, style::Reset);
        println!("{} | {:?} <--- {:?}", self.id, self.stack, byte);
        self.pc += 1;

        match *byte {
            Bytecode::PushI32(i) => {
                self.stack.push(Value::I32(i));
            },
            Bytecode::PushBool(b) => {
                self.stack.push(Value::Bool(b));
            },
            Bytecode::PushEffect(e, argc) => {
                self.stack.push(Value::Effect(e, argc));
            }
            Bytecode::PushArgc(argc) => {
                self.stack.push(Value::Argc(argc));
            }
            Bytecode::PushString(ref s) => {
                self.stack.push(Value::String(s.clone()));
            },

            Bytecode::Pop => {
                self.pop();
            }
            Bytecode::Load(pos) => {
                let value = self.get(pos).clone();

                self.stack.push(value);
            },
            Bytecode::Save(pos) => {
                let value = self.pop();

                self.set(pos, value);
            },
            Bytecode::SpawnAwait(pc) => {
                return self.spawn_await(pc);
            }
            Bytecode::Yield => {
                return self.handle_yield();
            }
            Bytecode::Continue => {
                return self.handle_continue();
            }
            Bytecode::Jump(pc) => {
                self.pc = pc;
            }
            Bytecode::Halt => {
                return Some(Async::Ready(()));
            }
            Bytecode::IsAlive => {
                let is_alive = self.child_context.is_some();
                self.stack.push(Value::Bool(is_alive))
            }
            Bytecode::Handle(_type, pc) => {
                let (effect, argc) = self.pop().expect_effect();
                if effect == _type {
                    self.pc = pc;
                } else {
                    self.stack.push(Value::Effect(effect, argc));
                }
            }
            Bytecode::SkipTrue => {
                let val: bool = self.pop().into();
                if val {
                    self.pc += 1;
                }
            }
            Bytecode::SkipFalse => {
                let val: bool = self.pop().into();
                if !val {
                    self.pc += 1;
                }
            }

            Bytecode::Print => {
                self.stack.push(Value::Effect(0, 1));
                return self.handle_yield();
            }
            Bytecode::Read => {
                self.stack.push(Value::Effect(1, 0));
                return self.handle_yield();
            }
        }

        None
    }

    fn get(&mut self, pos: usize) -> &Value {
        &self.stack[pos]
    }

    fn set(&mut self, pos: usize, value: Value) {
        self.stack[pos] = value;
    }

    fn spawn_await(&mut self, pc: usize) -> Option<Async<()>> {
        let (yield_tx, yield_rx) = mpsc::channel::<Event>();
        let (continue_tx, continue_rx) = mpsc::channel::<Event>();

        let parent_context = Context {
            tx: yield_tx,
            rx: continue_rx,
        };

        self.child_context = Some(Context {
            rx: yield_rx,
            tx: continue_tx,
        });

        let fiber = Fiber::new(pc, self.handle.clone(), self.code.clone())
            .with_parent(parent_context)
            .boxed();

        self.handle.spawn(fiber);

        self.state = State::Waiting(WaitFor::Yield);
        Some(Async::NotReady)
    }

    fn handle_yield(&mut self) -> Option<Async<()>> {
        let (_type, argc) = self.top().expect_effect();
        let popped = { (0..=argc).map(|_| self.pop()).collect::<Vec<Value>>() };

        let parent_context = self.parent_context.as_ref().expect("Expected parent fiber");

        parent_context.send(popped);

        self.state = State::Waiting(WaitFor::Continue);
        Some(Async::NotReady)
    }

    fn handle_continue(&mut self) -> Option<Async<()>> {
        let argc = self.pop().expect_argc();
        let popped = { (0..argc).map(|_| self.pop()).collect::<Vec<Value>>() };
        let child_context = self.child_context.as_ref().expect("Expected child fiber");

        child_context.send(popped);

        self.state = State::Waiting(WaitFor::Yield);
        Some(Async::NotReady)
    }

    fn top(&mut self) -> Value {
        self.stack.last().cloned().expect("Stack is empty")
    }

    fn pop(&mut self) -> Value {
        self.stack.pop().expect("Stack is empty")
    }
}

pub struct Context {
    tx: mpsc::Sender<Event>,
    rx: mpsc::Receiver<Event>,
}

impl Context {
    pub fn new(tx: mpsc::Sender<Event>, rx: mpsc::Receiver<Event>) -> Self {
        Context { tx, rx }
    }

    pub fn send(&self, val: Vec<Value>) {
        self.tx
            .send(Event::Continue(val))
            .expect("Cannot send value");
    }

    pub fn terminate(&self) {
        self.tx
            .send(Event::Terminate)
            .expect("Cannot terminate fiber");
    }

    pub fn terminated(&self) {
        self.tx
            .send(Event::Terminated)
            .expect("Cannot send info about terminated fiber");
    }
}
