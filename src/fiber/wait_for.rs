use super::{Fiber, State};
use futures::{stream::Stream, Async};

use crate::value::Value;

#[derive(Clone, Copy)]
pub enum WaitFor {
    Yield,
    Continue,
}

pub enum Event {
    Terminate,
    Terminated,
    Continue(Vec<Value>), // values
}

impl<H> Fiber<H> {
    pub fn wait_for(&mut self, wait_for: WaitFor) -> Result<Async<()>, ()> {
        let context = match wait_for {
            WaitFor::Yield => self.child_context.as_mut().expect("Child fiber"),
            WaitFor::Continue => self.parent_context.as_mut().expect("Parent fiber"),
        };

        let result = context.rx.poll()?;
        if let Async::Ready(Some(event)) = result {
            match event {
                Event::Continue(mut values) => {
                    values.reverse();
                    for val in values.into_iter() {
                        self.stack.push(val);
                    }
                    self.state = State::Running
                }
                Event::Terminated => {
                    self.child_context = None;
                    self.state = State::Running
                }
                Event::Terminate => {
                    println!("Terminate {}", self.id);
                    return Ok(Async::Ready(()));
                }
            }
        }
        Ok(Async::NotReady)
    }
}
