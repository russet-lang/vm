use fibers::Spawn;
use futures::{Async, Future};

use super::{Fiber, State};

impl<H> Future for Fiber<H>
where
    H: Spawn + Clone + Send + 'static,
{
    type Item = ();
    type Error = ();
    fn poll(&mut self) -> Result<Async<Self::Item>, Self::Error> {
        let result = match self.state {
            State::Running => self.run(),
            State::Waiting(wait_for) => self.wait_for(wait_for),
        };

        if let Ok(Async::Ready(_)) = result {
            if let Some(child_context) = self.child_context.as_ref() {
                child_context.terminate();
            }

            if let Some(parent_context) = self.parent_context.as_ref() {
                parent_context.terminated();
            }
        }

        result
    }
}
