use fibers::sync::mpsc;
use fibers::Spawn;
use futures::{stream::Stream, Async, Future};
use std::sync::Arc;

use std::collections::HashMap;

use core::bytecode::Bytecode;
use crate::fiber::{wait_for::Event, Context, Fiber};
use crate::value::Value;

/// `NativeHandler` - closure that handles native effects, for example terminal IO
type NativeHandler<'a> = Box<FnMut(mpsc::Sender<Event>, &[Value]) -> Async<()> + 'a>;

type EffectId = usize;

pub struct SourceCode(pub Vec<Bytecode>);

/// Root of virtual machine. It handles all IO and system calls, uses another fiber to do that.
/// It implements Future trait, so it can be used with any `Executor`
///
/// # Examples
///
/// ```rust
/// use fibers::{Executor, ThreadPoolExecutor};
/// use core::bytecode::Bytecode;
///
/// let mut executor = ThreadPoolExecutor::new().unwrap();
///
/// let code = vec![
///     Bytecode::PushI32(42)
/// ];
/// let mut vm = vm::Vm::new(executor.handle(), code);
/// vm.run();
/// executor.run_future(vm);
/// ```
pub struct Vm<'a, H> {
    code: Arc<SourceCode>,
    handle: H,
    main_rx: Option<mpsc::Receiver<Event>>,
    main_tx: Option<mpsc::Sender<Event>>,
    native_handlers: HashMap<EffectId, NativeHandler<'a>>,
}

impl<'a, H> Vm<'a, H> {
    /// Create new virtual machine with handle to `Executor`
    /// In most cases you want `ThreadPoolExecutor`
    ///
    /// # Examples
    ///
    /// ```rust
    /// use fibers::{Executor, ThreadPoolExecutor};
    ///
    /// let executor = ThreadPoolExecutor::new().unwrap();
    /// let code = vec![];
    /// let vm = vm::Vm::new(executor.handle(), code);
    /// ```
    pub fn new(handle: H, code: Vec<Bytecode>) -> Self {
        Vm {
            code: Arc::new(SourceCode(code)),
            handle,
            main_rx: None,
            main_tx: None,
            native_handlers: HashMap::new(),
        }
    }

    /// Add new native handler to virtual machine
    /// # Examples
    ///
    /// Adding new handler to vm which terminates fiber
    ///
    /// ```rust
    /// use futures::Async;
    /// use vm::fiber::wait_for::Event;
    /// use vm::value::Value;
    ///
    /// let mut vm = vm::vm_with_threadpool(vec![]);
    ///
    /// vm.add_handler(0, Box::new(|tx, values| {
    ///     println!("This code will run if effect with id 0 is received");
    ///     tx.send(Event::Terminate).unwrap();
    ///     Async::NotReady //Ready means "Exit program now"
    /// }));
    /// ```
    ///
    /// Resuming fiber with no value
    /// ```rust
    /// use futures::Async;
    /// use vm::fiber::wait_for::Event;
    /// use vm::value::Value;
    ///
    /// let mut vm = vm::vm_with_threadpool(vec![]);
    ///
    /// vm.add_handler(0, Box::new(|tx, values| {
    ///     tx.send(Event::Continue(vec![Value::Argc(0)])).unwrap();
    ///     Async::NotReady
    /// }));
    /// ```
    ///
    /// Resuming fiber with one value
    /// ```rust
    /// use futures::Async;
    /// use vm::fiber::wait_for::Event;
    /// use vm::value::Value;
    ///
    /// let mut vm = vm::vm_with_threadpool(vec![]);
    ///
    /// vm.add_handler(0, Box::new(|tx, values| {
    ///     let values = vec![
    ///         Value::Argc(1),
    ///         Value::I32(1234)
    ///     ];
    ///
    ///     tx.send(Event::Continue(values)).unwrap();
    ///     Async::NotReady
    /// }));
    /// ```
    pub fn add_handler(&mut self, id: EffectId, handler: NativeHandler<'a>) {
        self.native_handlers.insert(id, handler);
    }

    fn handle_effect(&mut self, _type: EffectId, values: &[Value]) -> Async<()> {
        if self.native_handlers.contains_key(&_type) {
            let tx = self.main_tx.clone().unwrap();
            let handler = self.native_handlers.get_mut(&_type).unwrap();
            handler(tx, values);
            return Async::NotReady;
        }

        println!("[core] Error: Effect not handled: {:?}", _type);
        Async::Ready(())
    }
}

impl<'a, H> Vm<'a, H>
where
    H: Spawn + Clone + Send + 'static,
{
    /// It spawns main fiber, creates channels for native handlers communication
    ///
    /// # Examples
    ///
    /// ```rust
    /// use fibers::{Executor, ThreadPoolExecutor};
    /// use core::bytecode::Bytecode;
    ///
    /// let mut executor = ThreadPoolExecutor::new().unwrap();
    ///
    /// let code = vec![
    ///     Bytecode::PushI32(42)
    /// ];
    /// let mut vm = vm::Vm::new(executor.handle(), code);
    /// //Add some handlers...
    /// // ...
    ///
    /// vm.run();
    ///
    /// executor.run_future(vm).unwrap().unwrap();
    pub fn run(&mut self) {
        let (yield_tx, yield_rx) = mpsc::channel::<Event>();
        let (continue_tx, continue_rx) = mpsc::channel::<Event>();

        let parent_context = Context::new(yield_tx, continue_rx);

        let main_fiber = Fiber::new(0, self.handle.clone(), self.code.clone())
            .with_parent(parent_context)
            .boxed();

        self.main_rx = Some(yield_rx);
        self.main_tx = Some(continue_tx);

        self.handle.spawn(main_fiber);
    }
}

impl<'a, H> Future for Vm<'a, H> {
    type Item = ();
    type Error = ();

    fn poll(&mut self) -> Result<Async<Self::Item>, Self::Error> {
        let ready = Async::Ready(());

        let result = self.main_rx.as_mut().unwrap().poll()?;

        let is_ready = if let Async::Ready(Some(event)) = result {
            match event {
                Event::Continue(values) => {
                    let effect = &values[0];
                    println!("[core] Continue!: {:?}", values);
                    let values = &values[1..];
                    let mut values = Vec::from(values);
                    values.reverse();
                    let (_type, _) = effect.expect_effect();
                    self.handle_effect(_type, &values)
                }
                Event::Terminated => {
                    println!("[core] Program terminated");
                    self.main_tx = None;
                    ready
                }
                _ => Async::NotReady,
            }
        } else {
            Async::NotReady
        };

        if let Async::Ready(_) = is_ready {
            println!("[core] Terminate!!!");
            if self.main_tx.is_some() {
                self.main_tx
                    .take()
                    .unwrap()
                    .send(Event::Terminate)
                    .expect("[core] Cannot terminate main fiber");
            }
        }

        Ok(is_ready)
    }
}
