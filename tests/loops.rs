use vm::{value::Value, driver::Driver};

#[test]
fn single_loop() {
    let printed = Driver::run_get_printed(r##"
let x :i32= 0;
print loop {
   if x > 5 { break };
   x = x + 1;
   print x
}
"##);

    assert_eq!(printed, vec![
        Value::I32(1),
        Value::I32(2),
        Value::I32(3),
        Value::I32(4),
        Value::I32(5),
        Value::I32(6)
    ]);
}

#[test]
fn break_value() {
    let printed = Driver::run_get_printed(r##"
let x:i32 = 0;
print loop {
   if x > 5 { break 7 };
   x = x + 1;
   print x
}
"##);

    assert_eq!(printed, vec![
        Value::I32(1),
        Value::I32(2),
        Value::I32(3),
        Value::I32(4),
        Value::I32(5),
        Value::I32(6)
    ]);
}

#[test]
fn break_value_to_variable() {
    let printed = Driver::run_get_printed(r##"
let x:i32 = 0;
let y:i32 = loop {
   if x > 5 { break (x * 2) };
   x = x + 1
};

print y
"##);

    assert_eq!(printed, vec![
        Value::I32(12)
    ]);
}
