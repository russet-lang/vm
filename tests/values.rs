use vm::{value::Value, driver::Driver};

#[test]
fn unsigned_integer() {
    let printed = Driver::run_get_printed("print 42");

    assert_eq!(printed, vec![Value::I32(42)]);
}

#[test]
fn signed_integer() {
    let printed = Driver::run_get_printed("print -42");

    assert_eq!(printed, vec![Value::I32(-42)]);
}

#[test]
fn boolean_true() {
    let printed = Driver::run_get_printed("print true");

    assert_eq!(printed, vec![Value::Bool(true)]);
}

#[test]
fn boolean_false() {
    let printed = Driver::run_get_printed("print false");

    assert_eq!(printed, vec![Value::Bool(false)]);
}

#[test]
fn strings() {
    let printed = Driver::run_get_printed(r#"   print "Ala ma kota" "#);

    assert_eq!(printed, vec![Value::String("Ala ma kota".into())]);
}
