use vm::{value::Value, driver::Driver};

#[test]
fn print() {
    let printed = Driver::run_get_printed(r##"
print 5;
print 6;
7
"##);

    assert_eq!(printed, vec![
        Value::I32(5),
        Value::I32(6)
    ]);
}
