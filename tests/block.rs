use vm::{value::Value, driver::Driver};

#[test]
fn root() {
    let printed = Driver::run_get_printed("print {2; 5}");

    assert_eq!(printed, vec![
        Value::I32(5)
    ]);
}

#[test]
fn implicit_block() {
    let printed = Driver::run_get_printed(r##"
{
2;
print 5
}
"##);

    assert_eq!(printed, vec![
        Value::I32(5)
    ]);
}

#[test]
fn empty_block() {
    let printed = Driver::run_get_printed("print { }");

    assert_eq!(printed, vec![]);
}
