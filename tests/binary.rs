use vm::{value::Value, driver::Driver};

#[test]
fn add_i32() {
    let printed = Driver::run_get_printed("print (2 + 3)");

    assert_eq!(printed, vec![
        Value::I32(5)
    ]);
}

#[test]
fn sub_i32() {
    let printed = Driver::run_get_printed("print (3 - 2)");

    assert_eq!(printed, vec![
        Value::I32(1)
    ]);
}


#[test]
fn add_many_i32() {
    let printed = Driver::run_get_printed("print (2 + 3 + 4)");

    assert_eq!(printed, vec![
        Value::I32(9)
    ]);
}

#[test]
fn mul_i32() {
    let printed = Driver::run_get_printed("print(2 * 3)");

    assert_eq!(printed, vec![
        Value::I32(6)
    ]);
}

#[test]
fn div_i32() {
    let printed = Driver::run_get_printed("print(10 / 2)");

    assert_eq!(printed, vec![
        Value::I32(5)
    ]);
}

#[test]
fn mul_many_i32() {
    let printed = Driver::run_get_printed("print (2 * 3 * 10)");

    assert_eq!(printed, vec![
        Value::I32(60)
    ]);
}

#[test]
fn many_i32_precedence() {
    let printed = Driver::run_get_printed("print (2 + 3 * 10)");

    assert_eq!(printed, vec![
        Value::I32(32)
    ]);
}

#[test]
fn many_i32_precedence_parent() {
    let printed = Driver::run_get_printed("print ((2 + 3) * 10)");

    assert_eq!(printed, vec![
        Value::I32(50)
    ]);
}

#[test]
fn many_i32_plus_unary() {
    let printed = Driver::run_get_printed("print (2 + 3 * -10)");

    assert_eq!(printed, vec![
        Value::I32(-28)
    ]);
}
