use vm::{value::Value, driver::Driver};


#[test]
fn assign() {
    let printed = Driver::run_get_printed(r##"
let x : i32 = 5;
x = 6;
print x
"##);

    assert_eq!(printed, vec![
        Value::I32(6)
    ]);
}
