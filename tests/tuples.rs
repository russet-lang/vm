use vm::{value::Value, driver::Driver};

#[test]
fn tuple_unit() {
    let printed:Vec<_> = Driver::run_get_printed(r##"
{
    let x: () = ();
    5;
    print x
}
"##); //.iter().map(|e| e.to_string()).collect();

    assert_eq!(printed, vec![]);
}

#[test]
fn singleton() {
    let printed:Vec<_> = Driver::run_get_printed(r##"
{
    let x: (i32) = (1);
    5;
    print x
}
"##); //.iter().map(|e| e.to_string()).collect();

    assert_eq!(printed, vec![Value::I32(1)]);
}

#[test]
fn tuple() {
    let printed:Vec<_> = Driver::run_get_printed(r##"
{
    let x: (i32, bool) = (1, true);
    5;
    print x
}
"##); //.iter().map(|e| e.to_string()).collect();

    assert_eq!(printed, vec![Value::I32(1), Value::Bool(true)]);
}

#[test]
fn typecheck_tuple() {
    let errors:Vec<_> = Driver::run_get_errors(r##"
{
    let x: (i32, bool) = "ala ma kota";
    print x
}
"##).iter().map(|e| e.to_string()).collect();

    assert_eq!(errors, vec![ "Mismatched types `(i32, bool)` != `string`".to_string()]);
}

#[test]
fn typecheck_inside_tuple() {
    let errors:Vec<_> = Driver::run_get_errors(r##"
{
    let x: (i32, bool) = ("ala ma kota", 5);
    print x
}
"##).iter().map(|e| e.to_string()).collect();

    assert_eq!(errors, vec![ "Mismatched types `(i32, bool)` != `(string, i32)`".to_string()]);
}
