use vm::{value::Value, driver::Driver};

#[test]
fn local_variable_decl() {
    let printed = Driver::run_get_printed(r##"
   let x: i32;
   print 5
"##);

    assert_eq!(printed, vec![
        Value::I32(5)
    ]);
}

#[test]
fn local_variable_usage() {
    let printed = Driver::run_get_printed(r##"
{
    let x: i32;
    6;
    x;
    print 5
}
"##);

    assert_eq!(printed, vec![
        Value::I32(5)
    ]);
}

#[test]
fn local_variable_with_assign() {
    let printed = Driver::run_get_printed(r##"
{
    let x: i32= 10;
    print x
}
"##);

    assert_eq!(printed, vec![
        Value::I32(10)
    ]);
}

#[test]
fn local_variable_with_assign_and_another_value() {
    let printed = Driver::run_get_printed(r##"
{
    let x:i32 = 10;
    6;
    print x
}
"##);

    assert_eq!(printed, vec![
        Value::I32(10)
    ]);
}

#[test]
fn local_variables() {
    let printed = Driver::run_get_printed(r##"
{
    let x : i32= 10, y :i32 = 11, z: i32 = 15;
    6;
    print y
}
"##);

    assert_eq!(printed, vec![
        Value::I32(11)
    ]);
}


#[test]
fn local_scope() {
    let printed = Driver::run_get_printed(r##"
{
    let x:i32 = 10;
    print {
        let x:i32 = 5;
        6;
        x
    }
}
"##);

    assert_eq!(printed, vec![
        Value::I32(5)
    ]);
}

#[test]
fn outer_scope() {
    let printed = Driver::run_get_printed(r##"
{
    let x:i32 = 10;
    print {
        let y:i32 = 5;
        6;
        x
    }
}
"##);

    assert_eq!(printed, vec![
        Value::I32(10)
    ]);
}


#[test]
fn outer_scope_2() {
    let printed = Driver::run_get_printed(r##"
{
    let x:i32 = 10;
    {
        let y:i32 = 5;
        6
    };
    print x
}
"##);

    assert_eq!(printed, vec![
        Value::I32(10)
    ]);
}
