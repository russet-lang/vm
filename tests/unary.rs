use vm::{value::Value, driver::Driver};

#[test]
fn negate_bool_false() {
    let printed = Driver::run_get_printed("print !false");

    assert_eq!(printed, vec![
        Value::Bool(true)
    ]);
}

#[test]
fn negate_bool_true() {
    let printed = Driver::run_get_printed("print !true");

    assert_eq!(printed, vec![
        Value::Bool(false)
    ]);
}

#[test]
fn wrong_unary() {
    let errors :Vec<String> = Driver::run_get_errors("!")
        .iter()
        .map(|e| e.to_string())
        .collect();

    assert_eq!(errors.len(), 1);
}
