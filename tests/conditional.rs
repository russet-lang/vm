use vm::{value::Value, driver::Driver};

#[test]
fn single_if() {
    let printed = Driver::run_get_printed(r##"
   print if true { 5 }
"##);

    assert_eq!(printed, vec![
        Value::I32(5)
    ]);
}

#[test]
fn else_if() {
    let printed = Driver::run_get_printed(r##"
   print if true { 5 } else { 10 }
"##);

    assert_eq!(printed, vec![
        Value::I32(5)
    ]);
}

#[test]
fn else_if2() {
    let printed = Driver::run_get_printed(r##"
   print if false { print 5; 6 } else { 10 }
"##);

    assert_eq!(printed, vec![
        Value::I32(10)
    ]);
}
