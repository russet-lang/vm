use vm::{driver::Driver};

#[test]
fn typecheck_let() {
    let errors:Vec<_> = Driver::run_get_errors(r##"
{
    let x: i32 = "ala ma kota";
    x
}
"##).iter().map(|e| e.to_string()).collect();

    assert_eq!(errors, vec![ "Mismatched types `i32` != `string`".to_string()]);
}

#[test]
fn typecheck_assign() {
    let errors:Vec<_> = Driver::run_get_errors(r##"
{
    let x: i32= 5;
    x = true;
    x
}
"##).iter().map(|e| e.to_string()).collect();

    assert_eq!(errors, vec![ "Mismatched types `i32` != `bool`".to_string()]);
}

#[test]
fn typecheck_if_branch() {
    let errors:Vec<_> = Driver::run_get_errors(r##"
{
    let x: i32 = 5;
    if x > 4 {
        "ala ma kota"
    } else { false }
}
"##).iter().map(|e| e.to_string()).collect();

    assert_eq!(errors, vec![ "Mismatched types `string` != `bool`".to_string()]);
}

#[test]
fn typecheck_if_cond() {
    let errors:Vec<_> = Driver::run_get_errors(r##"
{
    let x: i32= 5;
    if x {
        "ala ma kota"
    } else { false }
}
"##).iter().map(|e| e.to_string()).collect();

    assert_eq!(errors, vec![ "Expected type `bool` found `i32`".to_string()]);
}

#[test]
fn typecheck_binary_i32() {
    let errors:Vec<_> = Driver::run_get_errors(r##" 5 + true "##)
        .iter()
        .map(|e| e.to_string())
        .collect();

    assert_eq!(errors, vec!["Expected type `i32` found `bool`".to_string()]);
}

#[test]
fn typecheck_binary_i32_2() {
    let errors:Vec<_> = Driver::run_get_errors(r##" false + true "##)
        .iter()
        .map(|e| e.to_string())
        .collect();

    assert_eq!(errors, vec![ "Expected type `i32` found `bool`".to_string()]);
}

#[test]
fn typecheck_unary_i32() {
    let errors:Vec<_> = Driver::run_get_errors(r##" !5 "##)
        .iter()
        .map(|e| e.to_string())
        .collect();

    assert_eq!(errors, vec![ "Expected type `bool` found `i32`".to_string()]);
}

#[test]
fn typecheck_unit() {
    let errors:Vec<_> = Driver::run_get_errors(r##"
let x : bool = print "Ala ma kota"
"##)
        .iter()
        .map(|e| e.to_string())
        .collect();

    assert_eq!(errors, vec![ "Mismatched types `bool` != `()`".to_string()]);
}
